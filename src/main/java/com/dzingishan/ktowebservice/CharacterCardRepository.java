/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktowebservice;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author dzing
 */
public interface CharacterCardRepository extends CrudRepository<CharacterCard, Integer> {

    public CharacterCard findFirstByCardIdAndLocalization(int cardId, String localization);

    public List<CharacterCard> findAllByLocalization(String localization);

}
