/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktowebservice;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author dzing
 */
public interface UserRepository extends CrudRepository<User, Integer> {

    public User findFirstByEmail(String email);

    public boolean existsByEmail(String email);
    
    public User findFirstByUuid(int uuid);

}
