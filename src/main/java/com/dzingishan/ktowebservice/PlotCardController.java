/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktowebservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dzing
 */
@org.springframework.stereotype.Controller
public class PlotCardController {

    @Autowired
    private PlotCardRepository plotCardsRepo;

    @Autowired
    private CharacterCardRepository characterRepo;

    @RequestMapping("plot/loc")
    @ResponseBody
    public PlotCard getLocalization(@RequestParam("cardid") int cardId, @RequestParam("local") String localization) {
        PlotCard l = plotCardsRepo.findFirstByCardIdAndLocalization(cardId, localization);
        return l;
    }

    @RequestMapping("plot/list")
    @ResponseBody
    public Values getAllByLocal(@RequestParam(value = "local", defaultValue = "en", required = false) String localization) {
        return new Values(plotCardsRepo.findAllByLocalization(localization));
    }

    @RequestMapping("chars/loc")
    @ResponseBody
    public CharacterCard getCharacterCardLocalization(@RequestParam("cardid") int cardId, @RequestParam("local") String localization) {
        CharacterCard l = characterRepo.findFirstByCardIdAndLocalization(cardId, localization);
        return l;
    }

    @RequestMapping("chars/list")
    @ResponseBody
    public Values getAllCharactersByLocal(@RequestParam(value = "local", defaultValue = "en", required = false) String localization) {
        return new Values(characterRepo.findAllByLocalization(localization));
    }
}
