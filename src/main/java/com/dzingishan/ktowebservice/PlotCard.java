/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktowebservice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author dzing
 */
@Entity
public class PlotCard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private int uuid;

    private String localization;

    private int cardId;

    private String title;

    private String description;

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getUuid() {
        return uuid;
    }

    public String getTitle() {
        return title;
    }

    public String getLocalization() {
        return localization;
    }

    public String getDescription() {
        return description;
    }

    public int getCardId() {
        return cardId;
    }

}
